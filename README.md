Reflexology is a touch-based modality that brings balance from an imbalance in the body, mind and spirit. 
As a seasoned health and wellness expert, I will facilitate homeostasis  in your central nervous system. Add aromatherapy to further support balance in the CNS.

Address: 1624 Laskin Rd, #736, Virginia Beach, VA 23451

Phone: 323-428-8946